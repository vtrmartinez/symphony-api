const request = require("request");
var expect    = require("chai").expect;

var url = "https://api.publicapis.org/entries";

describe("Public API", function() {
    it("Returns status 200", function() {
        request(url, function(error, response, body) {
            expect(response.statusCode).to.equal(200);
        });
    });

    it("Find all objects with property - Category: Authentication Authorization", function() {
        request(url, function(error, response, body) {
            body_json = JSON.parse(body);
            category = 'Authentication & Authorization';

            for(var i=0; i < body_json['count']; i++){
                if(body_json['entries'][i]['Category'] == category){
                    obj_categories = (body_json['entries'][i]);
                    // Print found objects to console
                    // console.log(obj_categories);
                }
            }
        });
    });

    it("Read, Compare, count and verify number of objects", function() {
        request(url, function(error, response, body) {
            body_json = JSON.parse(body);
            
            var i = 0;
            var total = 0;
            var count = 0;

            while(body_json['entries'][i] != null){
                count ++;
                i++;
                if(count == body_json['count']){
                    expect(count).to.equal(body_json['count']);
                }

                // Print found objects to console
                // console.log(count);
            }
        });
    });

  });