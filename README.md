# Test - Symphony API

Installing and setting up, to execute the api tests using SuperTest, Mocha, and Chai with Javascript.

## Open the terminal and following the steps below:

* Pre requirement
    Install NodeJS:

      https://nodejs.org/en/download

    Install SuperTest:

        $ npm install supertest --save-dev

    Install Mocha and Chai:

        $ npm install --save mocha chai

    Install Request Framework

        $ npm install --save request

<br>
* Clone the project

    $ git clone https://gitlab.com/vtrmartinez/symphony-api


# Run the tests - locally

* Open the project's folder:

	 	$ cd ~/<project_folder>


* To run all the scenarios, use the command:

		$ npx mocha --reporter spec 

# Run the tests - CI Pipeline

- The CI pipeline on GitLab will be executed after every commit or, you can run it manually through the GitLab interface by clicking on Build > Pipelines > Run Pipeline.

<br>

## Heads Up!
To print the found objects to console, it's necessary to uncomment the lines commented